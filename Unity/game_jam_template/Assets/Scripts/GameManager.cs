﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    public int SplatMeter;
    public Text splatMeterText;

	void Start()
    {
        Time.timeScale = 0;
	}
	
	void Update()
    {
		if (Input.GetKeyDown(KeyCode.F1))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }

    public void GameStart()
    {
        Time.timeScale = 1;
        gameObject.GetComponent<AudioSource>().Play();
    }

    public void changeSplat (int change)
    {
        SplatMeter += change;

        if (SplatMeter < 0)
        {
            SplatMeter = 0;
        }
        splatMeterText.text = SplatMeter.ToString();
    }
}
