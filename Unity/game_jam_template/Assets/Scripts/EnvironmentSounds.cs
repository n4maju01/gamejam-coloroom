﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnvironmentSounds : MonoBehaviour {

    public AudioClip[] boingSounds;

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Projectile")
        {
            AudioSource.PlayClipAtPoint(boingSounds[Random.Range(0, boingSounds.Length)], col.transform.position);
        }
    }
}
