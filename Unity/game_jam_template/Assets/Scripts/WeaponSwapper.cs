﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponSwapper : MonoBehaviour {

    public GameObject[] weapons;
    private int currentIndex;

	void Start()
    {
        currentIndex = 1;
	}
	
	void Update()
    {
		if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            SwitchWeapon(0);
        }

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            SwitchWeapon(1);
        }

        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            SwitchWeapon(2);
        }

        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            SwitchWeapon(3);
        }
    }

    public void SwitchWeapon(int weaponIndex)
    {
        foreach(GameObject weapon in weapons) {
            weapon.SetActive(false);
        }

        currentIndex = weaponIndex - 1;
        weapons[weaponIndex].SetActive(true);
    }

    public void NextWeapon()
    {
        foreach (GameObject weapon in weapons)
        {
            weapon.SetActive(false);
        }

        currentIndex++;
        if (currentIndex == weapons.Length) {
            currentIndex = 0;
        }

        weapons[currentIndex].SetActive(true);
    }
}
