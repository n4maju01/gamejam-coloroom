﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour {

    public GameObject hitEffect;
    public GameObject splashLauncher;

    public AudioClip[] hitSounds;

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Projectile")
        {
            //Instantiate(hitEffect, col.transform.position, Quaternion.identity);
            col.gameObject.GetComponent<Projectile>().Explode();
            AudioSource.PlayClipAtPoint(hitSounds[Random.Range(0, hitSounds.Length)], col.transform.position);

            //splashLauncher.transform.position = col.transform.position;

            //Vector3 dir = col.contacts[0].normal;
            //dir = -dir.normalized;
            //splashLauncher.transform.LookAt(dir);
            splashLauncher.transform.LookAt(col.transform.position);
            //Debug.Log("IMP: " + col.impulse.magnitude);
            //Debug.Log("VEL: " + col.relativeVelocity.magnitude);

            float color = col.gameObject.GetComponent<Projectile>().gradientColor;
            splashLauncher.GetComponent<SplashLauncher>().EmitSplashes(color);
        }
    }
}
