﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyZone : MonoBehaviour {

    public GameManager manager;

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Projectile")
        {
            manager.changeSplat(-1);
            Destroy(col.gameObject);
        }
    }	
}
