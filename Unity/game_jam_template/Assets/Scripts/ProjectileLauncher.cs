﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileLauncher : MonoBehaviour {

    public GameObject projectile;
    public float ShootingRate = 2f;
    public float forceMin = 10f;
    public float forceMax = 50f;
    public float shootingConeRadius = 10f;
    public float aimZoneSize = 5f;
    public float aimZoneDistance = 5f;
    public AudioClip shootSound;

    void Start()
    {
        StartCoroutine("ShootProjectile");
    }

    private IEnumerator ShootProjectile()
    {
        while (true)
        {
            yield return new WaitForSeconds(ShootingRate);
            GameObject colorBall = Instantiate(projectile);
            colorBall.transform.position = transform.position;
            colorBall.transform.rotation = Quaternion.identity;
            colorBall.transform.RotateAround(colorBall.transform.position, transform.up, 180f);

            //Vector3 randomDir = RandomInCone(shootingConeRadius);
            //colorBall.transform.LookAt(randomDir);

            Vector3 direction = Random.insideUnitCircle * aimZoneSize;
            direction.z = aimZoneDistance;
            direction = transform.TransformDirection(direction.normalized);

            Rigidbody rb = colorBall.GetComponent<Rigidbody>();
            rb.velocity = direction * Random.Range(forceMin, forceMax);

            AudioSource.PlayClipAtPoint(shootSound, transform.position);
        }      
    }

    public static Vector3 RandomInCone(float radius)
    {
        //(sqrt(1 - z^2) * cosϕ, sqrt(1 - z^2) * sinϕ, z)
        float radradius = radius * Mathf.PI / 360;
        float z = Random.Range(Mathf.Cos(radradius), 1);
        float t = Random.Range(0, Mathf.PI * 2);
        return new Vector3(Mathf.Sqrt(1 - z * z) * Mathf.Cos(t), Mathf.Sqrt(1 - z * z) * Mathf.Sin(t), z);
    }
}
