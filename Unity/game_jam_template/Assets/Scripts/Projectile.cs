﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

    public float lifeTime = 5f;
    public Gradient Colors;
    public Color ballColor;
    public float gradientColor;

    private ParticleSystem ps;
    private ParticleSystem.MainModule main;
    private Collider ballCollider;
    private Renderer ballRenderer;
    private Rigidbody ballBody;
    private GameManager manager;
    private FusionRotator Handle;
    
	void Start()
    {
        manager = GameObject.FindWithTag("GameManager").GetComponent<GameManager>();
        Handle = GameObject.FindWithTag("Handle").GetComponent<FusionRotator>();
        gradientColor = Random.Range(0f, 1f);
        ballColor = Colors.Evaluate(gradientColor);
        gameObject.GetComponent<Renderer>().material.color = ballColor;
        ps = GetComponent<ParticleSystem>();

        main = ps.main;
        main.startColor = ballColor;

        ballCollider = GetComponent<Collider>();
        ballRenderer = GetComponent<Renderer>();
        ballBody = GetComponent<Rigidbody>();

        StartCoroutine("DestroyBall");
	}

    public void Explode()
    {
        ballCollider.enabled = false;
        ballRenderer.enabled = false;
        ballBody.Sleep();

        manager.changeSplat(1);

        if (Handle.hitBonus)
        {
            manager.changeSplat(1);
        }

        ps.Play();
        Destroy(gameObject, ps.main.duration);
    }

    private IEnumerator DestroyBall()
    {
        yield return new WaitForSeconds(lifeTime);
        manager.changeSplat(-1);
        Destroy(gameObject);
    }
}
