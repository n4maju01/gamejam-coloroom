﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplashLauncher : MonoBehaviour {

    public ParticleSystem particleLauncher;
    public ParticleSystem splatterParticles;
    public Gradient particleColorGradient;
    public ParticleDecalPool splatDecalPool;

    public AudioClip[] splashSounds;

    private float colorFloat;

    List<ParticleCollisionEvent> collisionEvents;

	void Start()
    {
        collisionEvents = new List<ParticleCollisionEvent>();
	}

    public void EmitSplashes(float color)
    {
        colorFloat = color;
        ParticleSystem.MainModule psMain = particleLauncher.main;
        psMain.startColor = particleColorGradient.Evaluate(colorFloat);
        particleLauncher.Emit(3);
    }

    void OnParticleCollision(GameObject other)
    {
        ParticlePhysicsExtensions.GetCollisionEvents(particleLauncher, other, collisionEvents);

        for (int i = 0; i < collisionEvents.Count; i++)
        {
            splatDecalPool.ParticleHit(collisionEvents[i], colorFloat);
            EmitAtLocation(collisionEvents[i]);
            AudioSource.PlayClipAtPoint(splashSounds[Random.Range(0, splashSounds.Length)], collisionEvents[i].intersection);
        }

    }

    void EmitAtLocation(ParticleCollisionEvent collisionEvent)
    {
        splatterParticles.transform.position = collisionEvent.intersection;
        splatterParticles.transform.rotation = Quaternion.LookRotation(collisionEvent.normal);

        ParticleSystem.MainModule psMain = splatterParticles.main;
        psMain.startColor = particleColorGradient.Evaluate(colorFloat);

        splatterParticles.GetComponent<SplatOnCollision>().colorFloat = colorFloat;
        splatterParticles.Emit(1);
    }
}
