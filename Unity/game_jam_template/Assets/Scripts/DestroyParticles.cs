﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyParticles : MonoBehaviour {

	void Start()
    {
        //GameObject effect = transform.GetChild(0).gameObject;
        //effect.SetActive(true);
        //ParticleSystem ps = effect.GetComponent<ParticleSystem>();
        //ps.Play();
        ParticleSystem ps = GetComponent<ParticleSystem>();
        ps.Play();
        Destroy(gameObject, ps.main.duration);
	}	
}
