﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplatOnCollision : MonoBehaviour {

	public ParticleSystem particleLauncher;
	public Gradient particleColorGradient;
	public ParticleDecalPool dropletDecalPool;
    public float colorFloat;

	List<ParticleCollisionEvent> collisionEvents;

	void Start() 
	{
		collisionEvents = new List<ParticleCollisionEvent>();
	}

	void OnParticleCollision(GameObject other)
	{
        ParticlePhysicsExtensions.GetCollisionEvents(particleLauncher, other, collisionEvents);

        for (int i = 0; i < collisionEvents.Count; i++)
        {
            dropletDecalPool.ParticleHit(collisionEvents[i], colorFloat);
        }
    }
}
