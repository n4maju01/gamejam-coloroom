﻿using UnityEngine;
using System.Collections;
using System;


public class FusionRotator : MonoBehaviour {
    private Rigidbody rb;
    public Quaternion myq;    
    private GameObject root;
    private Fusion myfusion;
    public bool lockPosition;
	
	public Quaternion prevMyq;
    
    public float maxX; // Debug
    public float maxY;
    public float maxZ;
    
    public Vector3 origPos;
    public Vector3 offsetPos; // This is the added coordinates to original position
    public Vector3 accChangedPos; // The two are used to calculate smooth movement
	public Vector3 acc;
	public Vector3 lastAccPos;
    public float maxOffset = 3.0f; // Maximum coordinate offset
    public float maxAccReturn = 35000.0f; // This should be 30k
	public float movementScale = 2.0f; // How much to scale the movement

    public WeaponSwapper swapper;
	public AudioClip[] wooshSounds;
		
	private bool rotChanged = false;
	private bool punchedForward = false;
	private bool punchedBackward = false;
	
	public int scoreToReset = 230;
    public bool hitBonus = false;

	private int currentScore = 0;
    private AudioSource audioSource;

    // Use this for initialization
    void Start () {
        rb = gameObject.GetComponent<Rigidbody>();
        root = GameObject.Find("sensorfusion");
        myfusion = root.GetComponent<Fusion>();
        audioSource = GetComponent<AudioSource>();

        myq = new Quaternion();
        lockPosition = false;
		
		prevMyq = new Quaternion();
        
        // InvokeRepeating("DebugRot", 0.5f, 0.5f)
        
        maxX = 0.0f;
        maxY = 0.0f;
        maxZ = 0.0f;
        
        origPos = rb.position;
		offsetPos = new Vector3(0.0f, 0.0f, 0.0f);
		lastAccPos = origPos;
    }
	
    // Update is called once per frame
	void Update () {        
        // get quaternion from sensor fusion
        myq = myfusion.getQuaternion();
		
		if (Mathf.Abs(Mathf.Abs(myq.x) - Mathf.Abs(prevMyq.x)) > 0.15f) {
			Debug.Log("WOWX " + Mathf.Abs(Mathf.Abs(myq.x) - Mathf.Abs(prevMyq.x)));
			PlayRandomSound();
			rotChanged = true;
            StartCoroutine("BonusTime");
			Invoke("ResetRotChanged", 1.0f);
		} else if (Mathf.Abs(Mathf.Abs(myq.y) - Mathf.Abs(prevMyq.y)) > 0.13f) {
			Debug.Log("WOWY " + Mathf.Abs(Mathf.Abs(myq.y) - Mathf.Abs(prevMyq.y)));
			PlayRandomSound();
			rotChanged = true;
            StartCoroutine("BonusTime");
            Invoke("ResetRotChanged", 1.0f);
		} else if (Mathf.Abs(Mathf.Abs(myq.z) - Mathf.Abs(prevMyq.z)) > 0.13f) {
			Debug.Log("WOWZ " + Mathf.Abs(Mathf.Abs(myq.z) - Mathf.Abs(prevMyq.z)));
			PlayRandomSound();
			rotChanged = true;
            StartCoroutine("BonusTime");
            Invoke("ResetRotChanged", 1.0f);
		} else if (Mathf.Abs(Mathf.Abs(myq.w) - Mathf.Abs(prevMyq.w)) > 0.13f) {
			Debug.Log("WOWW " + Mathf.Abs(Mathf.Abs(myq.w) - Mathf.Abs(prevMyq.w)));
			PlayRandomSound();
			rotChanged = true;
            StartCoroutine("bonusTime");
            Invoke("ResetRotChanged", 1.0f);
		}
		
		prevMyq = myq;
		

        try
        {
            rb.rotation = myq;
        }
        catch (Exception err)
        {
            print("error in q: " + myq.ToString() + "err: " + err.ToString());
        }
        
        acc = myfusion.getAccXYZ();
        
        // accChangedPos.x = movementScale * (((acc.x + lastAccPos.x) / 2.0f) / maxAccReturn);
        // accChangedPos.y = movementScale * (((acc.y + lastAccPos.y) / 2.0f) / maxAccReturn);
        // accChangedPos.z = movementScale * (((acc.z + lastAccPos.z) / 2.0f) / maxAccReturn);
        // accChangedPos.x = Mathf.Floor((movementScale * ((acc.x + lastAccPos.x) / 2.0f / maxAccReturn))*10.0f) / 10.0f;
        // accChangedPos.y = Mathf.Floor((movementScale * ((acc.y + lastAccPos.y) / 2.0f / maxAccReturn))*10.0f) / 10.0f;
        // accChangedPos.z = Mathf.Floor((movementScale * ((acc.z + lastAccPos.z) / 2.0f / maxAccReturn))*10.0f) / 10.0f;
        // accChangedPos.x = Mathf.Floor((movementScale * ((acc.x - lastAccPos.x) / maxAccReturn))*10.0f) / 10.0f;
        // accChangedPos.y = Mathf.Floor((movementScale * ((acc.y - lastAccPos.y) / maxAccReturn))*10.0f) / 10.0f;
        // accChangedPos.z = Mathf.Floor((movementScale * ((acc.z - lastAccPos.z) / maxAccReturn))*10.0f) / 10.0f;
        accChangedPos.x = movementScale * ((acc.x - lastAccPos.x) / maxAccReturn);
        accChangedPos.y = movementScale * ((acc.y - lastAccPos.y) / maxAccReturn);
        accChangedPos.z = movementScale * ((acc.z - lastAccPos.z) / maxAccReturn);
		
		// offsetPos.x -= accChangedPos.x;
		// offsetPos.y += accChangedPos.y;
		// offsetPos.z += accChangedPos.z;
		
		// cap the coordinates
		offsetPos.x = Mathf.Max(-maxOffset, offsetPos.x);
		offsetPos.x = Mathf.Min(maxOffset, offsetPos.x);
		// offsetPos.y = Mathf.Max(-maxOffset, offsetPos.y);
		// offsetPos.y = Mathf.Min(maxOffset, offsetPos.y);
		offsetPos.z = Mathf.Max(-maxOffset, offsetPos.z);
		offsetPos.z = Mathf.Min(maxOffset, offsetPos.z);
		
		
		
		// Debug.Log(
			// (Mathf.Abs(acc.x) - Mathf.Abs(lastAccPos.x)) + 
			// (Mathf.Abs(acc.y) - Mathf.Abs(lastAccPos.y)) + 
			// (Mathf.Abs(acc.z) - Mathf.Abs(lastAccPos.z))
		// );
		
		if (
		(Mathf.Abs(acc.x) - Mathf.Abs(lastAccPos.x)) + 
		(Mathf.Abs(acc.y) - Mathf.Abs(lastAccPos.y)) + 
		(Mathf.Abs(acc.z) - Mathf.Abs(lastAccPos.z)) < 1400) {
			currentScore++;
			// Debug.Log(currentScore);
			if (currentScore >= scoreToReset) {
				currentScore = 0;
				Debug.Log("RESET");
				myfusion.ResetPosition();
			}
		} else {
			currentScore = 0;
		}
		
		
        
        
        rb.position = new Vector3(origPos.x + offsetPos.x, origPos.y + offsetPos.y, origPos.z + offsetPos.z);
		// rb.position = new Vector3(origPos.x + accChangedPos.x, origPos.y + accChangedPos.y, origPos.z + accChangedPos.z);
		
		
		if (!rotChanged) {
			
			if ((lastAccPos.z - acc.z) < -20000 && !punchedBackward) {
				punchedForward = true;
				Invoke("ResetPunchedForward", 1.0f);
				Debug.Log("Forward");
			}
			
			if ((lastAccPos.z - acc.z) > 20000 && !punchedForward)
            {
                Debug.Log(lastAccPos.z - acc.z);
				Invoke("ResetPunchedBackward", 1.0f);
				Debug.Log("Backward");
                swapper.NextWeapon();
            }
			
			// if (lastAccPos.z - acc.z)
		}
		
		lastAccPos = acc;
		
        //Debug.Log(accChangedPos);
        
    }
	
	void PlayRandomSound(){
		//AudioSource.PlayClipAtPoint(wooshSounds[UnityEngine.Random.Range(0, wooshSounds.Length)], transform.position);
        audioSource.PlayOneShot(wooshSounds[UnityEngine.Random.Range(0, wooshSounds.Length)], 1f);
	}
	
	void ResetRotChanged() {
		rotChanged = false;
	}
	void ResetPunchedForward() {
		punchedForward = false;
	}
	void ResetPunchedBackward() {
		punchedBackward = false;
	}

    private IEnumerator BonusTime()
    {
        hitBonus = true;
        yield return new WaitForSeconds(0.5f);
        hitBonus = false;
    }
				
   
  void DebugRot() {
      
		if (Mathf.Abs(myq.x - prevMyq.x) > maxX) {
			maxX = Mathf.Abs(myq.x - prevMyq.x);
			Debug.Log("X: " + maxX);
		}

		if (Mathf.Abs(myq.y - prevMyq.y) > maxY) {
			maxY = Mathf.Abs(myq.y - prevMyq.y);
			Debug.Log("Y: " + maxY);
		}

		if (Mathf.Abs(myq.z - prevMyq.z) > maxZ) {
			maxZ = Mathf.Abs(myq.z - prevMyq.z);
			Debug.Log("Z: " + maxZ);
		}
		
		prevMyq = myq;
        
  }
}
