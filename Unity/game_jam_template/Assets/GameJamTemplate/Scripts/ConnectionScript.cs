﻿using UnityEngine;
using System.Threading;
using System;

public class ConnectionScript : MonoBehaviour
{
    private TCPConnection myTCP;
    private PipeConnection myPipe;
    private Rect rect;
    private GUIStyle style;
    private float timemultiplier = 1.0f;
    public static float timer;
    public static bool timeStarted = true;
    private string text;
    private int sampleCount = 0;
    private int odr = 100;
    private Thread networkThread = null;
    //private static System.Object networkThreadLock = new System.Object();

    int preSecondvalue = 0;

    private Fusion myfusion;
    private bool killthread = false;
    public bool connected = false;
    public GameManager manager;

    private bool useNamedPipe = false; // named pipe is used instead of socket

    void Awake()
    {
        // add copies of connection objects to this game object
        myTCP = gameObject.AddComponent<TCPConnection>();
        myPipe = gameObject.AddComponent<PipeConnection>();
        myfusion = gameObject.AddComponent<Fusion>();
    }

    void Start()
    {
        int w = Screen.width, h = Screen.height;
        style = new GUIStyle();
        rect = new Rect(60, 690, w, h * 2 / 100);
        style.alignment = TextAnchor.UpperLeft;
        style.fontSize = h * 2 / 50;
        style.normal.textColor = new Color(0.0f, 0.0f, 0.5f, 1f);
    }

    void OnDestroy()
    {
        try
        {
            if (useNamedPipe)
            {
                myPipe.ClosePipe();
            }
            killthread = true;

            if (networkThread != null && networkThread.IsAlive)
            {
                networkThread.Join(1000);
                print(String.Format("network thread finished: {0}", networkThread.ThreadState));
            }
            networkThread = null;
        }
        catch (Exception err)
        {
            Debug.Log("Error in OnDestroy: " + err.ToString());
        }

        print("ondestroy");        
    }
    void Update()
    {
        if (Input.GetKey("g"))
        {
            myfusion.resetGyroOffset();
        }
        if (timeStarted == true)
        {
            timer += Time.deltaTime * timemultiplier;
        }
    }

    void OnGUI()
    {
        if (useNamedPipe)
        {
            // if connection has not been made, display button to connect
            if (myPipe.pipeReady == false)
            {
                if (GUILayout.Button("Connect to pipe") || Input.GetKeyDown(KeyCode.Space))
                {
                    myPipe.SetupPipe();
                    startServer();
                    manager.GameStart();
                    // try to connect
                    Debug.Log("Attempting to connect..");
                }
            }

            if (myPipe.pipeReady == true)
            {
                connected = true;
            }
        }
        else
        {
            // if connection has not been made, display button to connect
            if (myTCP.socketReady == false)
            {
                if (GUILayout.Button("Connect to socket") || Input.GetKeyDown(KeyCode.Space))
                {
                    myTCP.setupSocket();
                    startServer();
                    manager.GameStart();
                    // try to connect
                    Debug.Log("Attempting to connect..");
                }
            }

            if (myTCP.socketReady == true)
            {
                connected = true;
            }
        }

        // display ODR and timer
        int minutes = Mathf.FloorToInt(timer / 60F);
        int seconds = Mathf.FloorToInt(timer - minutes * 60);

        if (seconds != preSecondvalue) { 
            preSecondvalue = seconds;

            text = string.Format("{0:0.0} s ({1:0.} odr)", seconds, sampleCount);

            if (sampleCount > 0)
            {
                odr = sampleCount;                    
            }
            sampleCount = 0;
        }
        GUI.Label(rect, text, style);
    }

    public void startServer()
    {       
        if (networkThread == null)
        {
            networkThread = new Thread(() =>
            {
                string serverSays;

                while (killthread == false)
                {
                    try
                    {
                        if (useNamedPipe)
                        {
                            serverSays = myPipe.ReadPipe();
                        }
                        else
                        {
                            serverSays = myTCP.readSocket();
                        }

                        if (serverSays != "")
                        {
                            //Debug.Log("serverSays:" + serverSays);
                            // caluculate sensor fusion
                            myfusion.calculateFusion(serverSays);
                            sampleCount++;
                        }
                        else
                        {
                            Thread.Sleep(1);
                        }
                    }
                    catch (Exception err)
                    {
                        Debug.Log("Error in network thread: " + err.ToString());
                        break;
                    }
                }

                //networkThread = null;
                print("bye from thread");
                connected = false;
            });
            networkThread.Start();
        }
    }
}
