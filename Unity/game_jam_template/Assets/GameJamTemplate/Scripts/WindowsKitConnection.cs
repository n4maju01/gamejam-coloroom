﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;
using System.Net.Sockets;
using System.Text;
using System.IO.Pipes;
using System.Threading;


public class TCPConnection : MonoBehaviour
{

    //the name of the connection, not required but better for overview if you have more than 1 connections running
    public string conName = "Localhost";

    //ip/address of the server, 127.0.0.1 is for your own computer
    public string conHost = "127.0.0.1";

    //port for the server, make sure to unblock this in your router firewall if you want to allow external connections
    public int conPort = 27015;

    //a true/false variable for connection status
    public bool socketReady = false;

    TcpClient mySocket;
    NetworkStream theStream;
    StreamWriter theWriter;
    StreamReader theReader;

    //try to initiate connection
    public void setupSocket()
    {
        try
        {
            mySocket = new TcpClient(conHost, conPort);
            //print("mySocket.ReceiveBufferSize: " + mySocket.ReceiveBufferSize);
            theStream = mySocket.GetStream();            
            theStream.ReadTimeout = 500;
            print("theStream.ReadTimeout: " + theStream.ReadTimeout);
            theWriter = new StreamWriter(theStream);
            theReader = new StreamReader(theStream);
            socketReady = true;
        }
        catch (Exception e)
        {
            Debug.Log("Socket error:" + e);
        }
    }

    ////send message to server
    //public void writeSocket(string theLine)
    //{
    //    if (!socketReady)
    //        return;
    //    String tmpString = theLine + "\r\n";
    //    theWriter.Write(tmpString);
    //    theWriter.Flush();
    //}

    //read message from server
    public string readSocket(int dataLength = 68) // 56
    {
        String result = "";
        int msglen = 0;

        if (theStream != null && theStream.DataAvailable)
        {
            //Byte[] inStream = new Byte[mySocket.SendBufferSize];            
            //theStream.Read(inStream, 0, inStream.Length);

            //Byte[] inStream = new Byte[mySocket.ReceiveBufferSize];
            Byte[] inStream = new Byte[dataLength];
            msglen += theStream.Read(inStream, 0, dataLength);

            result += System.Text.Encoding.UTF8.GetString(inStream);
            //result += inStream;
            //print("resp len " + result.Length.ToString());
            //Debug.Log("resp len " + result.Length.ToString());
        }
        //print("msglen: " + msglen);
        //if (result != "")
        //{
        //    print("socket data: " + result);
        //}
        return result;
    }

    //disconnect from the socket
    public void closeSocket()
    {
        if (!socketReady)
            return;
        theWriter.Close();
        theReader.Close();
        mySocket.Close();
        socketReady = false;
    }

    //keep connection alive, reconnect if connection lost
    public void maintainConnection()
    {
        if (!theStream.CanRead)
        {
            setupSocket();
        }
    }
}

public class PipeConnection : MonoBehaviour
{   
    //a true/false variable for connection status
    public bool pipeReady = false;

    public string pipeName = "KionixEvKitUnity";

    protected System.IO.Pipes.NamedPipeClientStream namedPipeClientStream;

    //try to initiate connection
    public void SetupPipe()
    {
        print("SetupPipe");

        try
        {
            if (namedPipeClientStream != null)
            {
                ClosePipe();
            }

            namedPipeClientStream = new NamedPipeClientStream(".", pipeName, PipeDirection.In);
            namedPipeClientStream.Connect();

            pipeReady = true;
        }
        catch (Exception e)
        {
            Debug.Log("Pipe error:" + e);
        }
    }

    //read message from server
    public string ReadPipe(int dataLength = 68) // 56
    {
        String result = "";
        int msglen = 0;

        try
        {
            if (namedPipeClientStream != null && namedPipeClientStream.IsConnected)
            {
                //print("ReadPipe");
                //Byte[] inStream = new Byte[mySocket.ReceiveBufferSize];
                Byte[] inStream = new Byte[dataLength];
                msglen += namedPipeClientStream.Read(inStream, 0, dataLength);

                result += System.Text.Encoding.UTF8.GetString(inStream);
                //result += inStream;
                //print("resp len " + result.Length.ToString());
                //Debug.Log("resp len " + result.Length.ToString());
            }
        }
        catch (Exception err)
        {
            Debug.Log("Error in reading pipe: " + err.ToString());
        }

        //print("msglen: " + msglen);
        //if (result != "")
        //{
        //    print("socket data: " + result);
        //}
        return result;
    }

    //disconnect from the socket
    public void ClosePipe()
    {
        print("ClosePipe");

        if (!pipeReady)
        {
            return;
        }

        try
        {
            //if (namedPipeClientStream != null && namedPipeClientStream.IsConnected)
            if (namedPipeClientStream != null)
            {
                //print("Stream closing");
                namedPipeClientStream.Close();
                namedPipeClientStream.Dispose();                
                namedPipeClientStream = null;                
            }
        }
        catch (Exception err)
        {
            print("ClosePipe error: " + err.ToString());
        }

        pipeReady = false;
    }
}
