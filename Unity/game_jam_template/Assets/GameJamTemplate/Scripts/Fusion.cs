﻿using UnityEngine;
using System.Collections;
using System;

public class Fusion : MonoBehaviour {
    private float gx, gy, gz, ax, ay, az, mx, my, mz;

    private float grx = 0, gry = 0, grz = 0; // value before offset correction
    private float gxo = 0, gyo = 0, gzo = 0; // offset value
    private float gtx, gty, gtz;             // temporary gyro values
    private float cfx = 0, cfy = 0, cfz = 0, cfw = 1; // "c" fusion quaternion
        
    private Quaternion cfusion;

    private float multiplier = 0.001090830782496456f; // for getting gyro raw data
    private static System.Object dataLock = new System.Object();
    private UInt64 samplecount = 0;

    public Quaternion myq;
    private Quaternion reset_myq;
    private Quaternion storedQuaternion;
    private Quaternion lastQuaternion;
    private Quaternion prevQuaternion;
    static private Quaternion origQuaternion = new Quaternion(0, 0, 0, 1);
    private bool gyroOffsetResetNeeded = true; // set to true when space pressed

    // Use this for initialization
    void Start () {
        myq = new Quaternion();

        myq.SetEulerRotation(0, 0, 0);
        cfusion = new Quaternion(0, 0, 0, 1);

        reset_myq = new Quaternion(0, 0, 0, 1);
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey("space"))
        {
			ResetPosition();
        }
	}
	
	public void ResetPosition () {
		gyroOffsetResetNeeded = true;     // enable automatic gyro offset reset on game startup

		print("resetpos");
		// store current quaternion taking cube position into account
		storedQuaternion = lastQuaternion;

		storedQuaternion = Quaternion.Inverse(storedQuaternion);
		reset_myq = storedQuaternion * origQuaternion;
	}

    float getQuaternionValue(string qString)
    {
        uint num = uint.Parse(qString, System.Globalization.NumberStyles.AllowHexSpecifier);
        byte[] floatVals = BitConverter.GetBytes(num);
        Array.Reverse(floatVals);
        return BitConverter.ToSingle(floatVals, 0);
    }

    void calculate(string serverSays)
    {
        samplecount += 1;

        // get Accelerometer data
        ax = (Int16)UInt16.Parse(serverSays.Substring(0, 4), System.Globalization.NumberStyles.HexNumber);
        ay = (Int16)UInt16.Parse(serverSays.Substring(4, 4), System.Globalization.NumberStyles.HexNumber);
        az = (Int16)UInt16.Parse(serverSays.Substring(8, 4), System.Globalization.NumberStyles.HexNumber);
        //print(ax.ToString() + "\t" + ay.ToString() + "\t" + az.ToString());

        // get Magnetometer data
        mx = (Int16)UInt16.Parse(serverSays.Substring(12, 4), System.Globalization.NumberStyles.HexNumber);
        my = (Int16)UInt16.Parse(serverSays.Substring(16, 4), System.Globalization.NumberStyles.HexNumber);
        mz = (Int16)UInt16.Parse(serverSays.Substring(20, 4), System.Globalization.NumberStyles.HexNumber);

        // get Gyroscope data
        gtx = (Int16)UInt16.Parse(serverSays.Substring(24, 4), System.Globalization.NumberStyles.HexNumber);
        gty = (Int16)UInt16.Parse(serverSays.Substring(28, 4), System.Globalization.NumberStyles.HexNumber);
        gtz = (Int16)UInt16.Parse(serverSays.Substring(32, 4), System.Globalization.NumberStyles.HexNumber);
        gx = gtx * multiplier;
        gy = gty * multiplier;
        gz = gtz * multiplier;

        grx = gx;gry = gy;grz = gz;
        gx -= gxo;gy -= gyo;gz -= gzo;
        //print(gx.ToString() + "\t" + gy.ToString() + "\t" + gz.ToString());

        // get sensor fusion quaternion values
        try
        {
            cfw = getQuaternionValue(serverSays.Substring(36, 8));
        }
        catch (Exception err)
        {
            Debug.Log("error: " + err.ToString());
            print("err w" + serverSays.Substring(36, 8));
        }

        try
        {
            cfx = getQuaternionValue(serverSays.Substring(44, 8));
        }
        catch (Exception err)
        {
            Debug.Log("error: " + err.ToString());
            print("err x" + serverSays.Substring(44, 8));
        }

        try
        {
            cfy = getQuaternionValue(serverSays.Substring(52, 8));
        }
        catch (Exception err)
        {
            Debug.Log("error: " + err.ToString());
            print("err y" + serverSays.Substring(52, 8));
        }

        try
        {
            cfz = getQuaternionValue(serverSays.Substring(60, 8));
        }
        catch (Exception err)
        {
            Debug.Log("error: " + err.ToString());
            print("err z" + serverSays.Substring(60, 8));
        }
        
        //print(String.Format("quaternion data: {0}, {1}, {2}, {3}", cfw, cfx, cfy, cfz));
        lock (dataLock)
        {
            myq.w = cfw;
            myq.x = -cfx;
            myq.y = -cfy;
            myq.z = cfz;

            // store last quaternion coming from fusion
            lastQuaternion = myq;

            // change quaternion according to stored position
            myq = reset_myq * myq;

            // calculate angle for gyro offset reset
            float quatAngle = Quaternion.Angle(prevQuaternion, myq);

            if (gyroOffsetResetNeeded && quatAngle <= 0.04)
            {
                //print("Angle: " + quatAngle);
                print("Automatic gyro offset reset done");
                resetGyroOffset();
                gyroOffsetResetNeeded = false;
            }

            // store quaternion for angle check
            prevQuaternion = myq;
        }
    }

    public void calculateFusion(string rawData)
    {
        calculate(rawData);   
    }

    public UInt64 getSampleCount()
    {
        return samplecount;
    }

    public void resetGyroOffset()
    {
        // sets current rotation speed to offset
        lock (dataLock)
        {
            float m = 1f;
            gxo = grx*m;
            gyo = gry*m;
            gzo = grz*m;
        }
        return;
    }

    public Vector3 getAccXYZ()
    {
        Vector3 acc = new Vector3();
        lock (dataLock)
        {
            acc.Set(ax, ay, az);
        }
        return acc;
    }

    public Vector3 getMagXYZ()
    {
        Vector3 mag = new Vector3();
        lock (dataLock)
        {
            mag.Set(mx, my, mz);
        }
        return mag;
    }

    public Vector3 getGyroXYZ()
    {
        Vector3 gyro = new Vector3();
        lock (dataLock)
        {
            gyro.Set(gx, gy, gz);
        }
        return gyro;
    }

    public Quaternion getQuaternion()
    {
        Quaternion q;
        lock (dataLock) {
            q = new Quaternion(myq.x, myq.y, myq.z, myq.w);
        }
        return q;
    }
}
